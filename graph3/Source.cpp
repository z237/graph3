#include <map>
#include <iostream>
#include <utility>
#include <cstdio>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <set>
#include <stack>

using namespace std;

class DSU {
public:
	int N;
	vector<int> p;
	DSU(int _N) : N(_N) {
		p.resize(_N);
		for (int i = 0; i < p.size(); i++) {
			p[i] = i;
		}
	}

	int find(int v) {
		return (v == p[v]) ? v : (p[v] = this->find(p[v]));
	}

	void unite(int a, int b) {
		a = this->find(a);
		b = this->find(b);
		if (rand() & 1) {
			swap(a, b);
		}
		if (a != b) {
			p[a] = b;
		}
	}
};

class Graph {

public:
	Graph() {
		W = R = false;
		M = 0; N = 0; type = 'E';
	}

	Graph(int _N) : N(_N) {
		W = R = false;
		M = 0; type = 'E';
	}

	void readGraph(string filename) {
		ifstream in(filename);
		in >> type;
		if (type == 'C') {
			in >> N >> R >> W;
			adjMatrix.resize(N, vector<int>(N, 0));

			int x;
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++) {
					in >> x;
					adjMatrix[i][j] = (W == 1) ? x : (x > 0) ? 1 : 0;
				}
			}
		}

		if (type == 'L') {
			in >> N >> R >> W;
			string buffer;
			getline(in, buffer);

			if (!W) {
				adjList.resize(N);
				for (int i = 0; i < N; i++) {
					getline(in, buffer);
					stringstream parser(buffer);
					int x;
					if (buffer == "") {
						continue;
					}
					while (!parser.eof() && buffer != "") {
						parser >> x;
						adjList[i].insert(x);
					}
				}
			}
			else {
				wAdjList.resize(N);
				for (int i = 0; i < N; i++) {
					getline(in, buffer);
					stringstream parser(buffer);
					int x, w;
					if (buffer == "") {
						continue;
					}
					while (!parser.eof()) {
						parser >> x >> w;
						wAdjList[i][x] = w;
					}
				}
			}
		}
		if (type == 'E') {
			in >> N >> M >> R >> W;
			if (!W) {
				int x, y;
				for (int i = 0; i < M; i++) {
					in >> x >> y;
					listOfEdges.insert(make_pair(x, y));
				}
			}
			else {
				int x, y, w;
				for (int i = 0; i < M; i++) {
					in >> x >> y >> w;
					wListOfEdges[make_pair(x, y)] = w;
				}
			}
		}
	}

	void addEdge(int from, int to, int weight) {
		M++;
		if (type == 'C') {
			from--; to--;
			adjMatrix[from][to] = weight;
			if (!R) {
				adjMatrix[to][from] = weight;
			}
		}
		if (type == 'L') {
			from--;
			if (!W) {
				adjList[from].insert(to);
				if (!R) {
					adjList[to].insert(from);
				}
			}
			else {
				wAdjList[from][to] = weight;
				if (!R) {
					wAdjList[to][from] = weight;
				}
			}
		}
		if (type == 'E') {
			if (!W) {
				listOfEdges.insert(make_pair(from, to));
			}
			else {
				wListOfEdges[make_pair(from, to)] = weight;
			}
		}
	}

	void removeEdge(int from, int to) {
		M--;
		if (type == 'C') {
			from--; to--;
			adjMatrix[from][to] = 0;
			M--;
			if (!R) {
				adjMatrix[to][from] = 0;
			}
		}
		if (type == 'L') {
			if (!W) {
				adjList[from - 1].erase(to);
				if (!R) {
					adjList[to - 1].erase(from);
				}
			}
			else {
				wAdjList[from - 1].erase(to);
				if (!R) {
					wAdjList[to - 1].erase(from);
				}
			}
		}
		if (type == 'E') {
			if (!W) {
				listOfEdges.erase(make_pair(from, to));
				if (!R) {
					listOfEdges.erase(make_pair(to, from));
				}
			}
			else {
				wListOfEdges.erase(make_pair(from, to));
				if (!R) {
					wListOfEdges.erase(make_pair(to, from));
				}
			}
		}
	}

	int changeEdge(int from, int to, int newWeight) {
		int prevWeight;
		if (type == 'C') {
			from--; to--;
			prevWeight = adjMatrix[from][to];
			adjMatrix[from][to] = newWeight;
			if (!R) {
				adjMatrix[to][from] = newWeight;
			}
		}
		if (type == 'L') {
			if (!W) {
				prevWeight = 0;
			}
			else {
				prevWeight = wAdjList[from - 1][to];
				wAdjList[from - 1][to] = newWeight;
				if (!R) {
					wAdjList[to - 1][from] = newWeight;
				}
			}
		}
		if (type == 'E') {
			if (!W) {
				prevWeight = 0;
			}
			else {
				if (R) {
					prevWeight = wListOfEdges[make_pair(from, to)];
					wListOfEdges[make_pair(from, to)] = newWeight;
				}
				else {
					if (wListOfEdges.find(make_pair(from, to)) != wListOfEdges.end()) {
						prevWeight = wListOfEdges[make_pair(from, to)];
						wListOfEdges[make_pair(from, to)] = newWeight;
					}
					else {
						prevWeight = wListOfEdges[make_pair(to, from)];
						wListOfEdges[make_pair(to, from)] = newWeight;
					}
				}
			}
		}
		return prevWeight;
	}

	void transformToAdjList() {
		if (type == 'L') {
			return;
		}
		if (!W) {
			adjList.resize(N);
		}
		else {
			wAdjList.resize(N);
		}

		if (type == 'C') {
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++) {
					if (adjMatrix[i][j] != 0) {
						if (!W) {
							adjList[i].insert(j + 1);
						}
						else {
							wAdjList[i].insert(make_pair(j + 1, adjMatrix[i][j]));
						}
					}
				}
			}
			adjMatrix.clear();
		}
		if (type == 'E') {
			if (!W) {
				for (auto x : listOfEdges) {
					adjList[x.first - 1].insert(x.second);
					if (!R) {
						adjList[x.second - 1].insert(x.first);
					}
				}
			}
			else {
				for (auto x : wListOfEdges) {
					wAdjList[x.first.first - 1].insert(make_pair(x.first.second, x.second));
					if (!R) {
						wAdjList[x.first.second - 1].insert(make_pair(x.first.first, x.second));
					}
				}
			}
			listOfEdges.clear();
			wListOfEdges.clear();
		}
		type = 'L';
	}

	void transformToAdjMatrix() {
		if (type == 'C') {
			return;
		}
		adjMatrix.resize(N, vector<int>(N, 0));
		if (type == 'L') {
			if (!W) {
				for (int i = 0; i < N; i++) {
					for (int x : adjList[i]) {
						adjMatrix[i][x - 1] = 1;
					}
				}
				adjList.clear();
			}
			else {
				for (int i = 0; i < N; i++) {
					for (auto x : wAdjList[i]) {
						adjMatrix[i][x.first - 1] = x.second;
					}
				}
				wAdjList.clear();
			}
		}
		if (type == 'E') {
			if (!W) {
				for (pair<int, int> x : listOfEdges) {
					adjMatrix[x.first - 1][x.second - 1] = 1;
					if (!R) {
						adjMatrix[x.second - 1][x.first - 1] = 1;
					}
				}
				listOfEdges.clear();
			}
			else {
				for (auto x : wListOfEdges) {
					adjMatrix[x.first.first - 1][x.first.second - 1] = x.second;
					if (!R) {
						adjMatrix[x.first.second - 1][x.first.first - 1] = x.second;
					}
				}
				wListOfEdges.clear();
			}
		}
		type = 'C';
	}

	void transformToListOfEdges() {
		if (type == 'E') {
			return;
		}

		if (type == 'C') {
			if (R) {
				for (int i = 0; i < N; i++) {
					for (int j = 0; j < N; j++) {
						if (adjMatrix[i][j] != 0) {
							if (!W) {
								listOfEdges.insert(make_pair(i + 1, j + 1));
							}
							else {
								wListOfEdges.insert(make_pair(make_pair(i + 1, j + 1), adjMatrix[i][j]));
							}
						}
					}
				}
			}
			else {
				for (int i = 0; i < N; i++) {
					for (int j = i; j < N; j++) {
						if (adjMatrix[i][j] != 0) {
							if (!W) {
								listOfEdges.insert(make_pair(i + 1, j + 1));
							}
							else {
								wListOfEdges.insert(make_pair(make_pair(i + 1, j + 1), adjMatrix[i][j]));
							}
						}
					}
				}
			}
			adjMatrix.clear();
		}

		if (type == 'L') {
			if (!W) {
				for (int i = 0; i < N; i++) {
					for (int x : adjList[i]) {
						if (!R) {
							listOfEdges.erase(make_pair(x, i + 1));
						}
						listOfEdges.insert(make_pair(i + 1, x));
					}
				}
				adjList.clear();
			}
			else {
				for (int i = 0; i < N; i++) {
					for (auto x : wAdjList[i]) {
						if (!R) {
							wListOfEdges.erase(make_pair(x.first, i + 1));
						}
						wListOfEdges[make_pair(i + 1, x.first)] = x.second;
					}
				}
				wAdjList.clear();
			}
		}


		if (!W) {
			M = listOfEdges.size();
		}
		else {
			M = wListOfEdges.size();
		}

		type = 'E';
	}

	void writeGraph(string filename) {
		ofstream out(filename);
		if (type == 'C') {
			out << type << ' ' << N << endl << R << ' ' << W << endl;
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++) {
					out << adjMatrix[i][j] << ' ';
				}
				out << endl;
			}
		}
		if (type == 'L') {
			out << type << ' ' << N << endl << R << ' ' << W << endl;
			if (!W) {
				for (int i = 0; i < N; i++) {
					for (int x : adjList[i]) {
						out << x << ' ';
					}
					out << endl;
				}
			}
			else {
				for (int i = 0; i < N; i++) {
					for (auto x : wAdjList[i]) {
						out << x.first << ' ' << x.second << ' ';
					}
					out << endl;
				}
			}
		}
		if (type == 'E') {
			out << type << ' ' << N << ' ' << M << endl << R << ' ' << W << endl;
			if (!W) {
				for (auto x : listOfEdges) {
					out << x.first << ' ' << x.second << endl;
				}
			}
			else {
				for (auto x : wListOfEdges) {
					out << x.first.first << ' ' << x.first.second << ' ' << x.second << endl;
				}
			}
		}

	}

	int checkEuler(bool &circleExist) {
		Graph g = *this;
		g.transformToAdjList();
		int oddVertex = 0;
		int start = 1;
		for (int i = 0; i < N; i++) {
			if (W == 0) {
				int deg = g.adjList[i].size();
				if (deg % 2 == 1) {
					oddVertex++;
					start = i + 1;
				}
			} else {
				int deg = g.wAdjList[i].size();
				if (deg % 2 == 1) {
					oddVertex++;
					start = i + 1;
				}
			}
		}
		if (oddVertex > 2) {
			return 0;
		}
		if (oddVertex == 0) {
			circleExist = true;
		}

		DSU dsu(N);

		for (int i = 0; i < N; i++) {
			int a = dsu.find(i);
			
			for (auto x : g.adjList[i]) {
				int b = dsu.find(x - 1);
				dsu.unite(a, b);
			}
				
		}
		bool possible = true;
		for (int i = 0; i < N - 1; i++) {
			if (dsu.p[i] != dsu.p[i + 1]) {
				possible = false;
			}
		}
		if (!possible) {
			return 0;
		}
		return start;
	}

	vector<int> getEuleranTourEffective() {
		bool circle;
		int start = checkEuler(circle);
		Graph g = *this;
		g.transformToAdjList();
		vector<int> seq;
		stack<int> s;
		s.push(start);
		while (!s.empty()) {
			int v = s.top();
			int next = 0;
			bool bridge = true;
			for (auto x : g.adjList[v - 1]) {
				if (g.adjList[x - 1].size() > 1) {
					bridge = false;
					next = x;
				}
			}
			if (bridge && g.adjList[v - 1].size() != 0) {
				auto tmp = g.adjList[v - 1].begin();
				next = *tmp;
			}
			if (next != 0) {
				g.removeEdge(v, next);
				s.push(next);
			}
			if (v == s.top()) {
				s.pop();
				seq.push_back(v);
			}
		}
		return seq;
	}


	void makeEuleran(int v, Graph &g, vector<int> &t) {
		int next = 0;
		for (auto x : g.adjList[v - 1]) {
			if (g.adjList[x - 1].size() > 1) {
				next = x;
			}
		}
		if (g.adjList[v - 1].size() > 0) {
			next = *(g.adjList[v - 1].begin());
		}
		if (g.M != 0) {
			g.removeEdge(next, v);
			makeEuleran(next, g, t);
		}
		t.push_back(v);
	}

	vector<int> getEuleranTourFleri() {
		bool circle;
		int start = checkEuler(circle);
		Graph g = *this;
		g.transformToAdjList();
		vector<int> tmp;
		makeEuleran(start, g, tmp);
		return tmp;
	}

	bool W, R;
	int N, M;
	char type;
	vector<vector<int> > adjMatrix;
	vector<set<int> > adjList;
	vector<map<int, int> > wAdjList;
	set<pair<int, int> > listOfEdges;
	map<pair<int, int>, int> wListOfEdges;
};

int main() {
	Graph G;
	G.readGraph("input.txt");
	vector<int> to = G.getEuleranTourFleri();
	for (int x : to) {
		cout << x << " ";
	}
	return 0;
}